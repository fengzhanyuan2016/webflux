package com.landation.blog.domain;

import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
@Document(collection = "article")
public class Article {
    @Id
    private String id;
    @NotBlank
    private String title;
    @NotBlank
    private String keyword;
    @NotBlank
    private String descript;
    @NotBlank
    private String tag;
    @NotBlank
    private String content;
    @NotBlank
    private String editContent;
    //状态 1 发布 2 草稿
    private Integer state = 1;
    //文章公开状态 1 公开 2 私密
    private Integer publish = 1;
    private String thumb;
    // 文章分类 1 code 2 think 3 民谣
    private Integer type = 1;
    private Date create_at = new Date();
    private Date update_at = new Date();


    private Integer meta_views = 0;
    private Integer meta_likes = 0;
    private Integer meta_comments = 0;


}
