package com.landation.blog.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document("hero")
@Data
public class Hero {
    @Id
    private String id;
    private String name;
    private String content;
    private Integer state = 1;
    private String ip;
    private String city;
    private String range;
    private String country;
    private String agent;
    private Date create_time = new Date();


    private String authorGravatar = "0";
    private String authorName;
    private String authorEmail;
    private String authorSite;









}
