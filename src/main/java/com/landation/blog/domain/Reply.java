package com.landation.blog.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document("reply")
@Data
public class Reply {
    @Id
    private String id;
    private String post_id;
    private String cid;
    private String from_gravatar;
    private String from_name;
    private String from_email;
    private String from_site;

    private String to_gravatar;
    private String to_name;
    private String to_email;
    private String to_site;

    private String content;
    private Integer likes;
    private String ip;
    private String city;
    private String range;
    private String country;

    private String agent;
    private Integer state =1;
    private Date create_at = new Date();
    private Date update_at = new Date();




}
