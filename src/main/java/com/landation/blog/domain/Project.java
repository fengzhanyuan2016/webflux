package com.landation.blog.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@Document("project")
@Data
public class Project {
    @Id
    private String id;
    @NotBlank
    private String title;
    @NotBlank
    private String descript;

    private String icon;
    private String view;
    private String githuab;
    private Date create_at= new Date();
    private Date update_at = new Date();


}
