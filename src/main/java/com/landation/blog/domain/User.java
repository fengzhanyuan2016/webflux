package com.landation.blog.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("user")
@Data
public class User {
    @Id
    private String id;
    private String name;
    private String username;
    private String slogan = "";
    private String gravatar;
    private String password;
    private Integer role = 1;

}
