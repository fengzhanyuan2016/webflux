package com.landation.blog.domain;

import lombok.Builder.Default;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
@Document(collection = "comment")
public class Comment {
    @Id
    private String id;
    @NotBlank
    private String post_id;
    private String article_url;
    private Integer pid = 0;
    @NotBlank
    private String content;
    private Integer likes = 0;
    private String ip;
    private String city;
    private String range;
    private String country;
    private String agent;

    private Integer state = 0;
    private Integer reply = 0;
    private Date create_at = new Date();
    private Date update_at = new Date();


    private String author_gravatar = "0";
    @NotBlank
    private String author_name;
    @NotBlank
    private String author_email;
    private String author_site;


}
