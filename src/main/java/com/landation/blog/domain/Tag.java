package com.landation.blog.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document("tag")
@Data
public class Tag {
    @Id
    private String id;
    private String name;
    private String descript;
    private Date create_at = new Date();
    private Date update_at = new Date();
    private Integer sort = 0;

}
