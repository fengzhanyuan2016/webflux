package com.landation.blog.repository;

import com.landation.blog.domain.Comment;
import com.landation.blog.domain.Reply;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface ReplyRepository extends ReactiveMongoRepository<Reply,String> {
    @Query("{'cid':?0}")
    Flux<Reply> FindReplysByCId(String postId);
}
