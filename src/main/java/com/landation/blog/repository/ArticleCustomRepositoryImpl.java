package com.landation.blog.repository;

import com.landation.blog.domain.Article;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import lombok.val;
import java.util.List;



public class ArticleCustomRepositoryImpl implements ArticleCustomRepository{
    public MongoTemplate mongoTemplate;
    public ArticleCustomRepositoryImpl(MongoTemplate mongoTemplate){
        this.mongoTemplate= mongoTemplate;
    }

    public List<Article> getNextArticles(String lastId, int size){

        final Criteria criteria = new Criteria();
        criteria.and("_id").lt(new ObjectId(lastId));
        final Query query = new Query(criteria).with(Sort.by(Sort.Direction.DESC,"_id"));
        query.limit(size);
        val list= mongoTemplate.find(query, Article.class);
        return list;
    }
}
