package com.landation.blog.repository;

import com.landation.blog.domain.Reply;
import com.landation.blog.domain.User;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface UserRepository extends ReactiveMongoRepository<User,String> {
    @Query("{'username':?0,'password':?1}")
    Mono<Reply> Login(String username, String password);
}
