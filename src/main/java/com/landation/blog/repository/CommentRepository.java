package com.landation.blog.repository;

import com.landation.blog.domain.Comment;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

public interface CommentRepository extends ReactiveMongoRepository<Comment,String> {
    @Query("{'post_id':?0}")
     Flux<Comment> FindCommentsByPostId(String postId);
}
