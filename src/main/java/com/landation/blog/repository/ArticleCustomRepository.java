package com.landation.blog.repository;

import com.landation.blog.domain.Article;

import java.util.List;

public interface ArticleCustomRepository {
    List<Article> getNextArticles(String lastId, int size);
}
