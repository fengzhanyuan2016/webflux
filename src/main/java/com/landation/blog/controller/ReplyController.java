package com.landation.blog.controller;

import com.landation.blog.domain.Comment;
import com.landation.blog.domain.Reply;
import com.landation.blog.repository.ArticleRepository;
import com.landation.blog.repository.CommentRepository;
import com.landation.blog.repository.ReplyRepository;
import lombok.val;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/reply")
public class ReplyController {




    private CommentRepository commentRepository;
    private ReplyRepository replyRepository;


    public ReplyController(CommentRepository commentRepository, ReplyRepository replyRepository) {
        this.commentRepository = commentRepository;
        this.replyRepository = replyRepository;
    }

    @PostMapping("add")
    public Mono<Reply> AddReply(ServerHttpRequest request, @RequestBody Reply reply) {


        //用户设备
        String agent = request.getHeaders().getFirst("user-agent");
        String permalink = "http://blog.ivenfeng.me/about";
        commentRepository.findById(reply.getPost_id())
                .flatMap(a-> {
                    val replys=a.getReply()+1;
                    a.setReply(replys);
                    return commentRepository.save(a);})

        ;
        reply.setAgent(agent);
        return replyRepository.save(reply);
    }

    @DeleteMapping("{id}")
    public Mono<ResponseEntity<Void>> deleteComment(@PathVariable String id) {
        val respnse = this.replyRepository.findById(id)
                .flatMap(a -> replyRepository.deleteById(id).then(Mono.just(new ResponseEntity<Void>(HttpStatus.OK))))
                .defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND));
        return respnse;
    }


    @PutMapping("{id}")
    public Mono<ResponseEntity<Reply>> updateComment(@PathVariable String id, @RequestBody Reply reply) {
        reply.setId(id);
        val response = replyRepository.findById(id)
                .flatMap(a -> {
                    return replyRepository.save(reply);
                })
                .map(a -> new ResponseEntity<Reply>(reply, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<Reply>(HttpStatus.NOT_FOUND));
        return response;
    }


    @GetMapping("{id}")
    public Flux<Reply> getCommentByCommentId(@PathVariable String cid) {
        val response = replyRepository.FindReplysByCId(cid);
        return response;
    }




}
