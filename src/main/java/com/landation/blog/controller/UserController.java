package com.landation.blog.controller;

import com.landation.blog.domain.User;
import com.landation.blog.repository.UserRepository;
import com.landation.blog.utils.Md5;
import lombok.val;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
public class UserController {
    private UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @PutMapping("/login")
    public Mono<ResponseEntity<Boolean>> login(@RequestBody User user) {
        val username = user.getUsername();
        val password = user.getPassword();
        val salt = "123456";


        val response = userRepository.Login(username, Md5.md5(password, salt))
                .hasElement()
                .map(a -> new ResponseEntity<Boolean>(true, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<Boolean>(HttpStatus.BAD_REQUEST));


        return response;
    }

    @GetMapping("/user/{id}")
    public Mono<ResponseEntity<User>> getArticleById(@PathVariable String id) {
        val response = userRepository.findById(id)
                .map(a -> new ResponseEntity<User>(a, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<User>(HttpStatus.NOT_FOUND));
        return response;
    }


}
