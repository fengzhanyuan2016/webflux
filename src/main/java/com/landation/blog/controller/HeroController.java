package com.landation.blog.controller;

import com.landation.blog.domain.Hero;
import com.landation.blog.repository.HeroRepository;
import lombok.val;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/hero")
public class HeroController {
    private HeroRepository heroRepository;
    public HeroController(HeroRepository heroRepository){
        this.heroRepository = heroRepository;
    }

    @PostMapping("add")
    public Mono<Hero> addHero(@RequestBody Hero hero){
        hero.setId(null);
       return this.heroRepository.save(hero);
    }

    @DeleteMapping("{id}")
    public Mono<ResponseEntity<Void>> deleteComment(@PathVariable String id) {
        val respnse = this.heroRepository.findById(id)
                .flatMap(a -> heroRepository.deleteById(id).then(Mono.just(new ResponseEntity<Void>(HttpStatus.OK))))
                .defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND));
        return respnse;
    }


    @PutMapping("{id}")
    public Mono<ResponseEntity<Hero>> updateComment(@PathVariable String id, @RequestBody Hero hero) {
        hero.setId(id);
        val response = heroRepository.findById(id)
                .flatMap(a -> {
                    return heroRepository.save(hero);
                })
                .map(a -> new ResponseEntity<Hero>(hero, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<Hero>(HttpStatus.NOT_FOUND));
        return response;
    }


    @GetMapping("")
    public Flux<Hero> getArticles() {
        return heroRepository.findAll(Sort.by(Sort.Direction.DESC,"_id"));
    }



}
