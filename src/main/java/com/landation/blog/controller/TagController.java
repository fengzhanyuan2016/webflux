package com.landation.blog.controller;

import com.landation.blog.domain.Project;
import com.landation.blog.domain.Tag;
import com.landation.blog.repository.TagRepository;
import lombok.val;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/tag")
public class TagController {
    private TagRepository tagRepository;
    public TagController(TagRepository tagRepository){
        this.tagRepository = tagRepository;
    }

    @PostMapping("add")
    public Mono<Tag> addHero(@RequestBody Tag tag){
        tag.setId(null);

        return this.tagRepository.save(tag);
    }

    @DeleteMapping("{id}")
    public Mono<ResponseEntity<Void>> deleteComment(@PathVariable String id) {
        val respnse = this.tagRepository.findById(id)
                .flatMap(a -> tagRepository.deleteById(id).then(Mono.just(new ResponseEntity<Void>(HttpStatus.OK))))
                .defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND));
        return respnse;
    }


    @PutMapping("{id}")
    public Mono<ResponseEntity<Tag>> updateComment(@PathVariable String id, @RequestBody Tag tag) {
        tag.setId(id);
        val response = tagRepository.findById(id)
                .flatMap(a -> {
                    return tagRepository.save(tag);
                })
                .map(a -> new ResponseEntity<Tag>(tag, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<Tag>(HttpStatus.NOT_FOUND));
        return response;
    }


    @GetMapping("")
    public Flux<Tag> getArticles() {
        return tagRepository.findAll(Sort.by(Sort.Direction.DESC,"_id"));
    }

}
