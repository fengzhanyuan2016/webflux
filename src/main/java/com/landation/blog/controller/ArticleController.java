package com.landation.blog.controller;

import com.landation.blog.domain.Article;
import com.landation.blog.repository.ArticleRepository;
import lombok.val;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/article")
public class ArticleController {

    private ArticleRepository articleRepository;
    public ArticleController(ArticleRepository articleRepository){
        this.articleRepository = articleRepository;
    }

    @PostMapping("add")
    public Mono<Article> addArticle(@RequestBody Article article) {
        article.setId(null);
        val art = articleRepository.save(article);
        return art;
    }

    @DeleteMapping("{id}")
    public Mono<ResponseEntity<Void>> deleteArticle(@PathVariable String id) {
        val respnse = this.articleRepository.findById(id)
                .flatMap(a -> articleRepository.deleteById(id).then(Mono.just(new ResponseEntity<Void>(HttpStatus.OK))))
                .defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND));
        return respnse;
    }

    @PutMapping("{id}")
    public Mono<ResponseEntity<Article>> updateArticle(@PathVariable String id, @RequestBody Article article) {
        article.setId(id);
        val response = articleRepository.findById(id)
                .flatMap(a -> {
                    return articleRepository.save(article);
                })
                .map(a -> new ResponseEntity<Article>(article, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<Article>(HttpStatus.NOT_FOUND));
        return response;
    }

    @GetMapping("{id}")
    public Mono<ResponseEntity<Article>> getArticleById(@PathVariable String id) {
        val response = articleRepository.findById(id)
                .map(a -> new ResponseEntity<Article>(a, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<Article>(HttpStatus.NOT_FOUND));
        return response;
    }

    @GetMapping("")
    public Flux<Article> getArticles() {
        return articleRepository.findAll(Sort.by(Sort.Direction.DESC,"_id"));
    }

    @GetMapping("next")
    public Flux<Article> getNextArticles(@RequestParam("lastId") String lastId, @RequestParam("pageSize") int pageSize) {
        val articleStream = articleRepository.getNextArticles(lastId,pageSize).stream();
        return Flux.fromStream(articleStream);
    }



}
