package com.landation.blog.controller;

import com.landation.blog.domain.Comment;
import com.landation.blog.repository.ArticleRepository;
import com.landation.blog.repository.CommentRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import lombok.val;

@RestController
@RequestMapping("/comment")
public class CommentController {
    private CommentRepository commentRepository;
    private ArticleRepository articleRepository;

    public CommentController(CommentRepository commentRepository, ArticleRepository articleRepository) {
        this.commentRepository = commentRepository;
        this.articleRepository = articleRepository;
    }

    @PostMapping("add")
    public Mono<Comment> AddComment(ServerHttpRequest request, @RequestBody Comment comment) {


        //用户设备
        String agent = request.getHeaders().getFirst("user-agent");
        String permalink = "http://blog.ivenfeng.me/about";
        articleRepository.findById(comment.getPost_id())
                .flatMap(a-> {
                    val comments=a.getMeta_comments()+1;
                    a.setMeta_comments(comments);
                    comment.setArticle_url("https://blog.naice.me/article/"+a.getId());
                    return articleRepository.save(a);})

        ;
        comment.setAgent(agent);
        return commentRepository.save(comment);
    }

    @DeleteMapping("{id}")
    public Mono<ResponseEntity<Void>> deleteComment(@PathVariable String id) {
        val respnse = this.commentRepository.findById(id)
                .flatMap(a -> commentRepository.deleteById(id).then(Mono.just(new ResponseEntity<Void>(HttpStatus.OK))))
                .defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND));
        return respnse;
    }


    @PutMapping("{id}")
    public Mono<ResponseEntity<Comment>> updateComment(@PathVariable String id, @RequestBody Comment comment) {
        comment.setId(id);
        val response = commentRepository.findById(id)
                .flatMap(a -> {
                    return commentRepository.save(comment);
                })
                .map(a -> new ResponseEntity<Comment>(comment, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<Comment>(HttpStatus.NOT_FOUND));
        return response;
    }


    @GetMapping("{id}")
    public Flux<Comment> getCommentByArticleId(@PathVariable String articleId) {
        val response = commentRepository.FindCommentsByPostId(articleId);
        return response;
    }










}
