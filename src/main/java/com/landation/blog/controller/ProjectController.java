package com.landation.blog.controller;

import com.landation.blog.domain.Hero;
import com.landation.blog.domain.Project;
import com.landation.blog.repository.ProjectRepository;
import lombok.val;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/project")
public class ProjectController {
    private ProjectRepository projectRepository;
    public ProjectController(ProjectRepository projectRepository){
        this.projectRepository= projectRepository;
    }


    @PostMapping("add")
    public Mono<Project> addHero(@RequestBody Project project){
        project.setId(null);
        return this.projectRepository.save(project);
    }

    @DeleteMapping("{id}")
    public Mono<ResponseEntity<Void>> deleteComment(@PathVariable String id) {
        val respnse = this.projectRepository.findById(id)
                .flatMap(a -> projectRepository.deleteById(id).then(Mono.just(new ResponseEntity<Void>(HttpStatus.OK))))
                .defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND));
        return respnse;
    }


    @PutMapping("{id}")
    public Mono<ResponseEntity<Project>> updateComment(@PathVariable String id, @RequestBody Project project) {
        project.setId(id);
        val response = projectRepository.findById(id)
                .flatMap(a -> {
                    return projectRepository.save(project);
                })
                .map(a -> new ResponseEntity<Project>(project, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<Project>(HttpStatus.NOT_FOUND));
        return response;
    }


    @GetMapping("")
    public Flux<Project> getArticles() {
        return projectRepository.findAll(Sort.by(Sort.Direction.DESC,"_id"));
    }




}
