package com.landation.blog.utils;

import org.springframework.util.DigestUtils;


/**
 * MD5通用类
 *
 * @author 浩令天下
 * @since 2017.04.15
 * @version 1.0.0_1
 *
 */
public class Md5 {
    /**
     * MD5方法
     *
     * @param text 明文
     * @param key 密钥
     * @return 密文
     * @throws Exception
     */
    public static String md5(String text, String key)  {
        //加密后的字符串
        try {
            byte[] bytes = (text + key).getBytes("UTF-8");
            String encodeStr= DigestUtils.md5DigestAsHex(bytes);
            System.out.println("MD5加密后的字符串为:encodeStr="+encodeStr);
            return encodeStr;
        }catch (Exception e){
            return null;
        }
    }

    /**
     * MD5验证方法
     *
     * @param text 明文
     * @param key 密钥
     * @param md5 密文
     * @return true/false
     * @throws Exception
     */
    public static boolean verify(String text, String key, String md5)  {
        //根据传入的密钥进行验证
        String md5Text = md5(text, key);
        if(md5Text.equalsIgnoreCase(md5))
        {
            System.out.println("MD5验证通过");
            return true;
        }

        return false;
    }
}
